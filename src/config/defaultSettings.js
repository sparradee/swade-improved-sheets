
export default {
    statusIcons: {
        'shaken':       '/modules/swade-improved-sheets/assets/pummeled.svg',
        'distracted':   '/modules/swade-improved-sheets/assets/distraction.svg',
        'vulnerable':   '/modules/swade-improved-sheets/assets/on-sight.svg',
        'stunned':      '/modules/swade-improved-sheets/assets/knockout.svg',
        'prone':        '/modules/swade-improved-sheets/assets/back-pain.svg',
        'entangled':    '/modules/swade-improved-sheets/assets/daemon-pull.svg',
        'bound':        '/modules/swade-improved-sheets/assets/handcuffed.svg',
        'berserking':   '/modules/swade-improved-sheets/assets/enrage.svg',
        'poisoned':     '/modules/swade-improved-sheets/assets/poison-bottle.svg',
        'diseased':     '/modules/swade-improved-sheets/assets/virus.svg',
        'bleedingOut':  '/modules/swade-improved-sheets/assets/heart-drop.svg',
        'onFire':       '/modules/swade-improved-sheets/assets/fire-silhouette.svg',
        'defending':    '/modules/swade-improved-sheets/assets/arrows-shield.svg',
        'unknown':      '/modules/swade-improved-sheets/assets/uncertainty.svg'
    },
    healthIcons: {
        'healthy':      '/modules/swade-improved-sheets/assets/strong.svg',
        'fatigued':     '/modules/swade-improved-sheets/assets/sleepy.svg',
        'wounded':      '/modules/swade-improved-sheets/assets/arm-sling.svg',
        'incapacitated':'/modules/swade-improved-sheets/assets/dead-head.svg'
    }
};
