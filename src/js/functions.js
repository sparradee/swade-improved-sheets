/**
 * Some various, random function for use throughout the site
 *
 */
function SwadeImprovedSheetsLog(...values)
{
    window.console.log('SWADE-Improved-Sheets |', ...values);
}


export { SwadeImprovedSheetsLog as log };
