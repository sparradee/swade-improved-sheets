/**
 * A proper "greater than" helper, because the SWADE one is actually
 * a "greater or equal than".
 *
 * @todo: Remove this on SWADE's next update (the incorrect gt has been corrected)
 */

Handlebars.registerHelper('gt', (lh, rh) => {
    return lh > rh;
});


/**
 * For debug use only; display JS data directly on a page.
 *
 * @example {{dump {settings: 'object'} }}
 */
Handlebars.registerHelper('dump', (val) => {
    return JSON.stringify(val, undefined, 4);
});
