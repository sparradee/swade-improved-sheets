import SwadeImprovedCharacterSheet from './sheets/swadeImprovedCharacterSheet.js';
import SwadeImprovedNPCSheet from './sheets/swadeImprovedNPCSheet.js';

import sisIconConfigurationMenu from './config/sisIconConfigurationMenu.js';

import DefaultSettings from '../config/defaultSettings.js';

import { log } from './functions.js';


/**
 * Module initialization; establish configuration options and pre-load html files
 * for later.
 */
Hooks.once('init', () => {
    log('Initializing');

    /**
     * Register the default settings for SIS.
     */
    game.settings.register('swade-improved-sheets', 'icon-settings', {
        name:   'swade-improved-sheets.configuration.options.name',
        hint:   'swade-improved-sheets.configuration.options.hint',
        scope:  'world',     // 'client', 'world'
        config: false,      // Do not show in the configuration pane
        type:   Object,
        default: {
            defaultIcons: DefaultSettings,
        },
        onChange: (val) => {
            log('DefaultSettings', val);

            /**
             * Set icon-settings to null to reset them to their defaults.
             */
            if (val === null) {
                game.settings.set('swade-improved-sheets', 'icon-settings', DefaultSettings);
            }
        }
    });


    /**
     * Create a configuration menu option for the Improved Sheets.
     */
    game.settings.registerMenu('swade-improved-sheets', 'icon-configuration-menu', {
        name:   'swade-improved-sheets.configuration.options.menu.name',
        label:  'swade-improved-sheets.configuration.options.menu.label',
        hint:   'swade-improved-sheets.configuration.options.menu.hint',
        icon:   'fas fa-cog',
        type:   sisIconConfigurationMenu,
        restricted: true    // GM only
    });


    /**
     * Pre-load and cache our template files.
     */
    loadTemplates([
        'modules/swade-improved-sheets/html/config/sisIconConfig.hbs',

        'modules/swade-improved-sheets/html/character/index.hbs',
        'modules/swade-improved-sheets/html/character/partials/header.hbs',
        'modules/swade-improved-sheets/html/character/partials/navbar.hbs',
        'modules/swade-improved-sheets/html/character/partials/mainBody.hbs',
        'modules/swade-improved-sheets/html/character/partials/summaryTab.hbs',
        'modules/swade-improved-sheets/html/character/partials/attributesPrimary.hbs',
        'modules/swade-improved-sheets/html/character/partials/attributesSecondary.hbs',

    ]);

});

/**
 * When the Game is ready to go...
 */
Hooks.once('ready', () => {
    log('Ready');
});

/* ==================================================================================================== */

log('Registering', 'New Sheets');
Actors.registerSheet("swade", SwadeImprovedCharacterSheet, {
    types: ["character"],
    makeDefault: true
});

Actors.registerSheet("swade", SwadeImprovedNPCSheet, {
    types: ["npc"],
    makeDefault: true
});

/* ==================================================================================================== */

// Done!
