import DefaultSettings from '../../config/defaultSettings.js';

import { log } from '../functions.js';

class sisIconConfigurationMenu extends FormApplication
{

    activateListeners = (html) => {
        super.activateListeners(html);
        log('Icon Configuration', 'Creating Listeners');

        /**
         * Pick new icons
         */
        html.find('.icon-picker').click((event) => {
            new FilePicker({
                type: "image",
                current: event.currentTarget.src,
                callback: (newFilePath) => {
                    event.currentTarget.src = newFilePath;
                }
            }).browse();
        });

        /**
         * Reset the icons to the default
         */
        html.find('.reset-button').click((event) => {
            event.preventDefault();
            this.onReset();
        });

        /**
         * Cancel
         */
        html.find('.cancel-button').click((event) => {
            event.preventDefault();
            this.close();
        });

    }

    /**
     * Data for the HTML
     */
    getData = () => {
        return game.settings.get('swade-improved-sheets', 'icon-settings');
    }

    /**
     * The "Reset" configuration button; set all the status icons back to their default
     * options.
     */
    onReset = () => {
        log('Icon-Configuration', 'Reset');

        $('form.sis.icon-config .icon-picker.health-icons').each((index, item) => {
            const data = $(item).data();    // {icon, source: <url>, edit: <name>}
            const defaultUrl = DefaultSettings.healthIcons[data.icon];
            $(item).data('source', defaultUrl).attr('src', defaultUrl);
        });
        $('form.sis.icon-config .icon-picker.status-icons').each((index, item) => {
            const data = $(item).data();    // {icon, source: <url>, edit: <name>}
            const defaultUrl = DefaultSettings.statusIcons[data.icon];
            $(item).data('source', defaultUrl).attr('src', defaultUrl);
        });

    }

    /**
     * Save the new icon settings
     */
    async _updateObject(event, formData) {
        // formData is automatically assembled as {<data-edit>: <src>}

        log('Icon-Configuration', 'UpdateObject', formData);

        let newSettingsObject = { healthIcons: {}, statusIcons: {} };
        Object.keys(formData).forEach((key) => {
            const [category, iconKey] = key.split('.');
            newSettingsObject[category][iconKey] = formData[key];
        });

        await game.settings.set('swade-improved-sheets', 'icon-settings', newSettingsObject);
        ui.notifications.info('Icon Settings Saved');

        // // Force any open character sheets to re-render
        // game.actors.map((a) => a.sheet._state === 2 ? a.render(true) : null)

    }

    static get defaultOptions()
    {
        return mergeObject(super.defaultOptions, {
            title:         'swade-improved-sheets.configuration.options.iconConfig.title',
            id:            'swade-improved-sheets-icon-config',
            template:      '/modules/swade-improved-sheets/html/config/sisIconConfig.hbs',
            width:         500,
            height:        705,
            closeOnSubmit: true
        });
    }

}

export default sisIconConfigurationMenu;
