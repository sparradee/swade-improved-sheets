import SwadeNPCSheet from '/systems/swade/module/sheets/SwadeNPCSheet.js';

import { log } from '../functions.js';

class SwadeImprovedNPCSheet extends SwadeNPCSheet
{

    /**
     * Assemble the data to pass to the html.
     */
    getData = () => {
        /**
         * Settings for module awareness:
         *
         *      betterrolls-swade:      If this is enabled, then DON'T show the skill icons, permitting BR to add its own icon instead
         *
         */

        let swadeImprovedSheetsSettings = {
            swadeImprovedSheets: mergeObject({
                modulesInUse: {
                    'betterroll-swade': game.modules.has('betterrolls-swade') ? game.modules.get('betterrolls-swade').active : false
                }
            }, game.settings.get('swade-improved-sheets', 'icon-settings'))
            /**
             * This adds:
             *  {
             *      healthIcons: [],
             *      statusIcons: []
             *  }
             */
        };

        let inheritedSettings = super.getData();

        const data = mergeObject(inheritedSettings, swadeImprovedSheetsSettings);
        return data;
    }

}

export default SwadeImprovedNPCSheet;
