import SwadeCharacterSheet from '/systems/swade/module/sheets/SwadeCharacterSheet.js';

import { log } from '../functions.js';


class SwadeImprovedCharacterSheet extends SwadeCharacterSheet
{

    statusTrayIsOpen = false;

    /**
     * Replace the default template with out own!
     */
    get template() {
        return 'modules/swade-improved-sheets/html/character/index.hbs';
    }


    /**
     * Assemble the data to pass to the html.
     */
    getData = () => {
        /**
         * Settings for module awareness:
         *
         *      betterrolls-swade:      If this is enabled, then DON'T show the skill icons, permitting BR to add its own icon instead
         *
         */

        let swadeImprovedSheetsSettings = {
            swadeImprovedSheets: mergeObject({
                modulesInUse: {
                    betterrollsSwade: game.modules.has('betterrolls-swade') ? game.modules.get('betterrolls-swade').active : false
                },
                statusTrayIsOpen: this.statusTrayIsOpen,
            }, game.settings.get('swade-improved-sheets', 'icon-settings'))
            /**
             * This adds:
             *  {
             *      healthIcons: [],
             *      statusIcons: []
             *  }
             */
        };

        let inheritedSettings = super.getData();

        const data = mergeObject(inheritedSettings, swadeImprovedSheetsSettings);
        return data;
    }


    /**
     *
     */
    activateListeners = (html) => {
        super.activateListeners(html);



        // Skills

            // Swap the functionality of the 'label' and 'value' click events.
            html.find('.skill-label a').off('click');

            // Roll Skill
            html.find('.skills-list .skill-value a, .skills-list .skill-image').on('click', (event) => {
                let element = event.currentTarget;
                let item = element.parentElement.parentElement.parentElement.dataset.itemId;
                this.actor.rollSkill(item, { event: event });
            });


        /**
         * For non-editable sheets, return now. Everything else below is
         * for editing.
         */
        if (!this.options.editable) { return; }

        // Statuses

            html.find('.toggle-status-tray, .status-icon-area > img').on('click', () => {
                this.statusTrayIsOpen = !this.statusTrayIsOpen;

                let tray = html.find('.status-tray');
                if (this.statusTrayIsOpen) {
                    tray.addClass('is-open');
                } else {
                    tray.removeClass('is-open');
                }
            });

            html.find('.clear-all-statuses').on('click', () => {
                this.actor.update({
                    'data.status.isShaken': false,
                    'data.status.isDistracted': false,
                    'data.status.isVulnerable': false,
                    'data.status.isStunned': false,
                    'data.status.isProne': false,
                    'data.status.isEntangled': false,
                    'data.status.isBound': false,
                    'data.status.isBerserking': false,
                    'data.status.isPoisoned': false,
                    'data.status.isDiseased': false,
                    'data.status.isBleedingOut': false,
                    'data.status.isOnFire': false,
                    'data.status.isDefending': false,
                    'data.status.isOtherStatus': false,
                });
            });

        // CONVICTION

            // Add Conviction
            html.find('.conviction-add').on('click', () => {
                const current = this.actor.data.data.details.conviction.value;
                this.actor.update({
                    'data.details.conviction.value': current + 1,
                });
            });

            // Remove Conviction
            html.find('.conviction-subtract').on('click', () => {
                const current = this.actor.data.data.details.conviction.value;
                if (current > 0) {
                    this.actor.update({
                        'data.details.conviction.value': current - 1,
                    });
                }
            });

    }


}

export default SwadeImprovedCharacterSheet;
