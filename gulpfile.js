const gulp = require('gulp');
const fs = require('fs-extra');
const path = require('path');
const chalk = require('chalk');

const sass = require('gulp-sass');
const archiver = require('archiver');

/********************/
/*      CLEAN       */
/********************/

    /**
     * Empty the 'dist/' folder.
     *
     * This will delete fonts/, swade-improved-sheets.css
     */
    async function clean() {
        const name  = path.basename(path.resolve('.')); // 'swade-improved-sheets'
        let files = ['fonts', 'assets', 'html', 'config', 'lang', 'js'];

        // Delete the root css file ('swade-improved-sheets.css')
        if (fs.existsSync(path.join('src', `${name}.scss`))) {
            files.push(`${name}.css`);
        }

        console.log(' ', chalk.yellow('Deleting:'));
        console.log('   ', chalk.blueBright('dist/' + files.join('\n    dist/')));

        // Attempt to remove the files
        try {
            for (const filePath of files) {
                await fs.remove(path.join('dist', filePath));
            }
            return Promise.resolve();
        } catch (err) {
            Promise.reject(err);
        }

    }


/********************/
/*  UPDATE MANIFEST */
/********************/

    /**
     * @todo: Automate changing the module.json version property to match the package.json ?
     */
    async function updateManifest(callback) {
        return Promise.resolve();
    }


    /**
     * Fetch the module.json FoundryVTT manifest file for use here.
     *
     * @returns {Object} {file: <module.json content>, name: 'module.json', root: 'src'}
     */
    function getModuleManifest() {
        let json = {
            root: 'src',
            file: null,
            name: ''
        };

        const moduleConfigFileName = 'module.json';
        const moduleConfigFile = path.join(json.root, moduleConfigFileName);

        if (fs.existsSync(moduleConfigFile)) {
            json.file = fs.readJSONSync(moduleConfigFile);
            json.name = moduleConfigFileName;
        } else {
            return;
        }

        return json;
    }


/********************/
/*      BUILD       */
/********************/

    /**
     * These are the steps to fully build this module.
     */
    const buildModule = gulp.parallel(
        compileSASS,
        copyStaticFiles,
    );


    /**
     * Build SASS
     */
    async function compileSASS() {
        return Promise.resolve(gulp
            .src('src/*.scss')
            .pipe(sass({outputStyle: 'compact'}).on('error', sass.logError))
            .pipe(gulp.dest('dist'))
        );
    }

    /**
     * Copy static files (images, html/handlbars, etc.)
     */
    async function copyStaticFiles(statics = []) {
        if (!statics.length) {
            statics = ['fonts', 'assets', 'html', 'config', 'js', 'lang', 'module.json'];
        }

        try {
            for (const file of statics) {
                if (fs.existsSync(path.join('src', file))) {
                    fs.copySync(path.join('src', file), path.join('dist', file));
                }
            }
            console.log(chalk.yellow('  Copied ' + statics.join() + ' folder(s)'));
            return Promise.resolve();
        } catch (err) {
            console.error(chalk.red(err));
            return Promise.reject(err);
        }

    }


/********************/
/*     PACKAGE      */
/********************/

    /**
     * Create a distributable .zip file of the module.
     */
    async function packageBuild() {
        const manifest = getModuleManifest();

        return new Promise((resolve, reject) => {
            try {
                // Remove the package dir without doing anything else
                console.log(chalk.yellow(' Removing all packaged files'));
                fs.removeSync('package');

                // Ensure there is a directory to hold all the packaged versions
                fs.ensureDirSync('package');

                // Create the archive
                const zipName = `${manifest.file.name}.zip`;
                const zipFile = fs.createWriteStream(path.join('package', zipName));
                const zip = archiver('zip', { zlib: { level: 9 } });

                zipFile.on('close', () => {
                    console.log(chalk.green(zip.pointer() + ' total bytes'));
                    console.log(chalk.green(`Zip file ${zipName} has been written`));
                    return resolve();
                });

                zip.on('error', (err) => {
                    throw err;
                });

                zip.pipe(zipFile);

                // Add the directory with the final code
                zip.directory('dist/', manifest.file.name);

                zip.finalize();

            } catch (error) {
                return reject(error);
            }
        });
    }

/********************/
/*    WATCHERS      */
/********************/


    /**
     * Set up a watch routine to check for changes to SCSS files, or any file
     * in fonts/, assets/, config/, js/, and html/
     */
    function startWatchers() {
        // Any changes to a SASS file...
        gulp.watch(['src/*.scss', 'src/**/*.scss'], { ignoreInitial: false, delay: 500 }, compileSASS);

        // Watch for infrequently-changed content
        gulp.watch(['src/fonts', 'src/assets', 'src/config'], { ignoreInitial: false, delay: 500 }, (cb) => cb(copyStaticFiles(['fonts', 'assets', 'config'])));

        // Watch for frequently changed content
        gulp.watch(['src/html'], { ignoreInitial: false, delay: 500 }, (cb) => cb(copyStaticFiles(['html'])));
        gulp.watch(['src/lang'], { ignoreInitial: false, delay: 500 }, (cb) => cb(copyStaticFiles(['lang'])));
        gulp.watch(['src/js'],   { ignoreInitial: false, delay: 500 }, (cb) => cb(copyStaticFiles(['js'])));

    }

    function startDevWatcher() {
        gulp.watch(['dist'], { ignoreInitial: true, delay: 500 }, copyToDevEnvironment);
    }

    async function copyToDevEnvironment(cb) {
        const privateData = fs.readJSONSync('private.json');
        if (privateData && privateData.devPath) {
            console.log(chalk.magenta('  Copying all files to ' + privateData.devPath));
            fs.copySync(path.join('dist'), privateData.devPath, {overwrite: true});
        }
        return cb();
    }

/********************/
/*     EXPORTS      */
/********************/
exports.clean   = clean;
exports.build   = gulp.series(clean, updateManifest, buildModule);
exports.watch   = startWatchers;
exports.package = packageBuild;
exports.publish = gulp.series(
    clean,
    updateManifest,
    buildModule,
    packageBuild,
    // execGit,
);

exports.dev     = gulp.parallel(
    startWatchers,
    startDevWatcher
);

// not ready
exports.update  = updateManifest; // NYI

async function linkUserData() { console.log('linkUserData'); return Promise.resolve(); }

// exports.default = () => {};
exports.link = linkUserData;
