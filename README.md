# SWADE Improved Sheets

This is still an early prototype, used as a learning exercise.

This module updates aspects of the default SWADE actor sheets.

## Changes

### Character Sheet

* Rearranged a few elements, and resized some columns.
* Added an icon to represent the health of the character, based on their Fatigue and Wound level.
* Moved the Status checkboxes to a slide-out tray, and added many of the other SWADE statuses as well. There is also a user-fillable field for any other less common status.
* Added icons for each status type.
* Added a "Run" clickable, moving the click event from the 'Pace' header to the new icon.
* In the Summary tab, added the skill icon to the Skill listing as well as the die type icon. Also swapped the click functionality, so now clicking on the die (or icon) opens the roll dialog, and clicking on the skill name opens the skill dialog.

### Configuration

* Implemented a configuration window for gamemasters to change the character status icons and health icons.

## Credits

Default status icons courtesy of https://game-icons.net/
